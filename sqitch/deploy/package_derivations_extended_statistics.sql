-- Deploy guix-data-service:package_derivations_extended_statistics to pg

BEGIN;

CREATE STATISTICS package_derivations_mcv (mcv) ON system_id, target FROM package_derivations;

CREATE STATISTICS package_derivations_ndistinct (ndistinct) ON system_id, target FROM package_derivations;

COMMIT;
