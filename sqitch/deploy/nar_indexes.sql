-- Deploy guix-data-service:nar_indexes to pg

BEGIN;

CREATE INDEX narinfo_signatures_narinfo_signature_data_id ON narinfo_signatures (narinfo_signature_data_id);;;;

CREATE INDEX nar_references_nar_id ON nar_references (nar_id);

COMMIT;
