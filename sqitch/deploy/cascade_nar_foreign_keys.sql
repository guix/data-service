-- Deploy guix-data-service:cascade_nar_foreign_keys to pg

BEGIN;

ALTER TABLE narinfo_fetch_records
  DROP CONSTRAINT narinfo_fetch_records_narinfo_signature_data_id_fkey,
  ADD CONSTRAINT narinfo_fetch_records_narinfo_signature_data_id_fkey
  FOREIGN KEY (narinfo_signature_data_id) REFERENCES narinfo_signature_data(id)
  ON DELETE CASCADE;

ALTER TABLE narinfo_signatures
  DROP CONSTRAINT narinfo_signatures_nar_id_fkey,
  ADD CONSTRAINT narinfo_signatures_nar_id_fkey
  FOREIGN KEY (nar_id) REFERENCES nars(id)
  ON DELETE CASCADE;

ALTER TABLE narinfo_signatures
  DROP CONSTRAINT narinfo_signatures_narinfo_signature_data_id_fkey,
  ADD CONSTRAINT narinfo_signatures_narinfo_signature_data_id_fkey
  FOREIGN KEY (narinfo_signature_data_id) REFERENCES narinfo_signature_data(id)
  ON DELETE CASCADE;

ALTER TABLE nar_references
  DROP CONSTRAINT "nar_references_nar_id_fkey",
  ADD CONSTRAINT "nar_references_nar_id_fkey"
  FOREIGN KEY (nar_id) REFERENCES nars(id)
  ON DELETE CASCADE;

ALTER TABLE nar_urls
  DROP CONSTRAINT "nar_urls_nar_id_fkey",
  ADD CONSTRAINT "nar_urls_nar_id_fkey"
  FOREIGN KEY (nar_id) REFERENCES nars(id)
  ON DELETE CASCADE;

COMMIT;
