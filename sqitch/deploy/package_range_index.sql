-- Deploy guix-data-service:package_range_index to pg

BEGIN;

CREATE INDEX range_package_name_idx ON package_derivations_by_guix_revision_range (package_name);

COMMIT;
