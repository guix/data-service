-- Deploy guix-data-service:blocked_builds to pg

BEGIN;

CREATE TABLE blocked_builds (
  build_server_id integer NOT NULL REFERENCES build_servers (id),
  blocked_derivation_output_details_set_id integer NOT NULL REFERENCES derivation_output_details_sets (id),
  blocking_derivation_output_details_set_id integer NOT NULL REFERENCES derivation_output_details_sets (id),
  PRIMARY KEY (
    build_server_id,
    blocked_derivation_output_details_set_id,
    blocking_derivation_output_details_set_id
  )
) PARTITION BY LIST (build_server_id);

CREATE INDEX blocked_builds_blocked_derivation_output_details_set_id
  ON blocked_builds (build_server_id, blocked_derivation_output_details_set_id);

COMMIT;
