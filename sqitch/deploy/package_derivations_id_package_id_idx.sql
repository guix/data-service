-- Deploy guix-data-service:package_derivations_id_package_id_idx to pg

BEGIN;

CREATE INDEX package_derivations_id_package_id_idx ON package_derivations (id, package_id) WITH (fillfactor='100');

COMMIT;
