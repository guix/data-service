-- Deploy guix-data-service:blocked_builds_blocked_builds_blocked_derivation_output_details_set_id_2 to pg

BEGIN;

CREATE INDEX blocked_builds_blocked_derivation_output_details_set_id_2
  ON blocked_builds (blocked_derivation_output_details_set_id);

COMMIT;
