-- Deploy guix-data-service:derivation_outputs_id_and_derivation_id_idx to pg

BEGIN;

CREATE INDEX derivation_outputs_id_and_derivation_id ON derivation_outputs (id, derivation_id);

COMMIT;
