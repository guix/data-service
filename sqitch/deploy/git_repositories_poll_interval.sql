-- Deploy guix-data-service:git_repositories_poll_interval to pg

BEGIN;

ALTER TABLE git_repositories
  ADD COLUMN poll_interval INTEGER DEFAULT NULL;

COMMIT;
