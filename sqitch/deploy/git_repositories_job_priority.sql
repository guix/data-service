-- Deploy guix-data-service:git_repositories_job_priority to pg

BEGIN;

ALTER TABLE git_repositories ADD COLUMN job_priority INTEGER NOT NULL DEFAULT 0;

COMMIT;
