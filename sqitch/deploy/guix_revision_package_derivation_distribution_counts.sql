-- Deploy guix-data-service:guix_revision_package_derivation_distribution_counts to pg

BEGIN;

CREATE TABLE guix_revision_package_derivation_distribution_counts (
  guix_revision_id integer NOT NULL REFERENCES guix_revisions (id),
  system_id integer NOT NULL REFERENCES systems (id),
  target varchar NOT NULL,
  level integer NOT NULL,
  distinct_derivations integer NOT NULL
);

COMMIT;
