(define-module (test-model-git-commit)
  #:use-module (srfi srfi-19)
  #:use-module (srfi srfi-64)
  #:use-module (guix-data-service database)
  #:use-module (guix-data-service model git-repository)
  #:use-module (guix-data-service model git-branch)
  #:use-module (guix-data-service model git-commit))

(test-begin "test-model-git-commit")

(with-postgresql-connection
 "test-module-git-commit"
 (lambda (conn)
   (check-test-database! conn)

   (test-assert "insert-git-commit-entry works"
     (with-postgresql-transaction
      conn
      (lambda (conn)
        (let* ((url "test-url")
               (git-repository-id
                (git-repository-url->git-repository-id conn url))
               (git-branch-id
                (insert-git-branch-entry conn git-repository-id "master")))
          (insert-git-commit-entry conn
                                   git-branch-id
                                   "test-commit"
                                   (current-date)))
        #t)
      #:always-rollback? #t))

   (test-assert "insert-git-commit-entry works twice"
     (with-postgresql-transaction
      conn
      (lambda (conn)
        (let* ((url "test-url")
               (git-repository-id
                (git-repository-url->git-repository-id conn url))
               (git-branch-id
                (insert-git-branch-entry conn git-repository-id "master")))
          (insert-git-commit-entry conn
                                   git-branch-id
                                   "test-commit"
                                   (current-date))
          (insert-git-commit-entry conn
                                   git-branch-id
                                   "test-commit"
                                   (current-date)))
        #t)
      #:always-rollback? #t))))

(test-end)
