;;; Guix Data Service -- Information about Guix over time
;;; Copyright © 2019 Christopher Baines <mail@cbaines.net>
;;;
;;; This program is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU Affero General Public License
;;; as published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.
;;;
;;; This program is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Affero General Public License for more details.
;;;
;;; You should have received a copy of the GNU Affero General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

(define-module (guix-data-service model license)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-43)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 match)
  #:use-module (squee)
  #:use-module (guix inferior)
  #:use-module (guix-data-service database)
  #:use-module (guix-data-service model utils)
  #:export (inferior-packages->license-id-lists
            inferior-packages->license-data))

(define inferior-package-id
  (@@ (guix inferior) inferior-package-id))

(define (inferior-packages->license-data inf)
  (define proc
    `(vector-map
      (lambda (_ package)
        (match (package-license package)
          ((? license? license)
           (list
            (list (license-name license)
                  (license-uri license)
                  (license-comment license))))
          ((values ...)
           (map (match-lambda
                  ((? license? license)
                   (list (license-name license)
                         (license-uri license)
                         (license-comment license)))
                  (x
                   (simple-format
                    (current-error-port)
                    "error: unknown license value ~A for package ~A"
                    x package)
                   #f))
                values))
          (x
           (simple-format
            (current-error-port)
            "error: unknown license value ~A for package ~A"
            x package)
           '())))
      gds-inferior-packages))

  (inferior-eval '(use-modules (guix licenses)) inf)
  (inferior-eval proc inf))

(define (inferior-packages->license-id-lists conn license-data)
  (define (string-or-null v)
    (if (string? v)
        v
        ;; save non string values as NULL
        NULL))

  (vector-map
   (lambda (_ license-tuples)
     (if (null? license-tuples)
         #()
         (insert-missing-data-and-return-all-ids
          conn
          "licenses"
          `(name uri comment)
          (list->vector
           (filter-map
            (match-lambda
              ((name uri comment)
               (list name
                     (string-or-null uri)
                     (string-or-null comment)))
              (#f #f))
            license-tuples)))))
   license-data))
